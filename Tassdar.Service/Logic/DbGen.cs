﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Tassdar.Core.Helper;
using Tassdar.Service.Entity;

namespace Tassdar.Service.Logic
{
   public class DbGen:DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("Data Source=localhost;user id=root;password=123456;Initial Catalog=tassdar");
        }
        public DbSet<UserEnitty> Users { get; set; }
    }
}
