﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tassdar.Core.Helper
{
    public static class DIHelper
    {
        public static IServiceProvider ServiceProvider;

        public static object GetService<IService>()
        {
            return ServiceProvider.GetService(typeof(IService));
        }
    }
}
