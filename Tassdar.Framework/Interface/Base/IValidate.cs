﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tassdar.Framework.Interface.Base
{
    /// <summary>
    /// 实体验证接口，用于在持久化数据之前进行数据的验证
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
   public interface IValidate<TEntity>
    {
        (bool result, string message) Validate(TEntity entity);
    }
}
